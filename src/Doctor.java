public class Doctor extends MedicalStaff{
    private String specialty;
    public Doctor(String name, int age, String socialSecurityNumber, String employeeId, String specialty) {
        super(name, age, socialSecurityNumber, employeeId);
        this.specialty = specialty;
    }

    @Override
    public String careForPatient(Patient patient) {
        return "Doctor " + getName() + " cares for " + patient.getName();
    }

    @Override
    public String getRole() {
        return "Doctor";
    }
}
