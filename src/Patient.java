import java.util.ArrayList;
import java.util.List;

public class Patient extends Person{
    private String patientId;
    private List<Illness> illnessList = new ArrayList<>();
    public Patient(
            String name,
            int age,
            String socialSecurityNumber,
            String patientId
    ) {
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
    }

    public void addIllness(Illness illness){
        this.illnessList.add(illness);
    }

    public String getInfo(){
        StringBuilder illnessName = new StringBuilder();
        for(Illness illness : illnessList){
            illnessName.append(illness.getInfo());
        }
        return "Détails du patient :" + "\n"
                + getName() + ", " + getAge() + "ans" + "\n"
                + "Sécurité sociale : " + getSocialSecurityNumber() + "\n"
                + "------------" + "\n"
                + "Maladie du patient :"  + "\n" + illnessName;
    }
}
