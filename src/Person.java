public abstract class Person {
    private String name;
    private int age;
    private String socialSecurityNumber;

    public Person(
            String name,
            int age,
            String socialSecurityNumber
    ){
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public String getName(){
        return this.name;
    }
    public int getAge(){
        return this.age;
    }
    public String getSocialSecurityNumber(){
        return this.socialSecurityNumber;
    }
}
