public class Nurse extends MedicalStaff{
    public Nurse(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber, employeeId);
    }

    @Override
    public String careForPatient(Patient patient) {
        return "Nurse " + getName() + " cares for " + patient.getName();
    }

    @Override
    public String getRole() {
        return "Nurse";
    }
}
