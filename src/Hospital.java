public class Hospital {
    public static void main(String[] args){
        //On crée des instances de médicament
        Medication doliprane = new Medication("Doliprane", "500g");

        //On instancie de nouvelles maladies
        Illness fievre = new Illness("Fièvre jaune");
        Illness echarde = new Illness("Écharde dans le pied");
        //On ajoute un medicament pour cette maladie
        fievre.addMedication(doliprane);

        //On instancie des patients
        Patient antoine = new Patient("Antoine Chacha", 28, "2346 5676 6898 657G", "256S");
        Patient marine = new Patient("Marine Thotho", 25, "6628 9373 1223 365F", "1089U");
        //On ajoute la maladie au patient
        antoine.addIllness(fievre);
        marine.addIllness(echarde);
        //On affiche les informations du patient
        System.out.println(antoine.getInfo());

        //On instancie un docteur
        Doctor pinel = new Doctor("Frédéric Pinel", 52, "5436 6879 8303 321G", "12A", "Neurochirurgien");
        System.out.println("\n" + "=> " + pinel.careForPatient(antoine));

        System.out.println("_____________________________________" + "\n");

        System.out.println(marine.getInfo());
        //On instancie une infirmière
        Nurse bosc = new Nurse("Christine Bosc", 56, "6548 0938 5342 254F", "38C");
        System.out.println("\n" + "=> " + bosc.careForPatient(marine));

    }
}
