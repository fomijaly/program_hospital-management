public interface Care {
    String careForPatient(Patient patient);
    default void recordPatientVisit(String notes){
        System.out.println(notes);
    }
}
