import java.util.*;

public class Illness {
    private String name;
    private List<Medication> medicationList = new ArrayList<>();
    public Illness(
            String name
    ){
       this.name = name;
    }
    public void addMedication(Medication medication){
        this.medicationList.add(medication);
    }

    public String getInfo(){
        StringBuilder medicationName = new StringBuilder();
        //On boucle sur le tableau des medicaments
        for(Medication medication : medicationList){
            medicationName.append(medication.getInfo());
        }
        return "- " + this.name + "\n"
                + medicationName;
    }
}
